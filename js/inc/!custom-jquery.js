jQuery(window).bind("load", function() {
  jQuery(function($) {

    //Animate On Scroll Enabled
    AOS.init();

    //iLightbox Enabled
    if ($("a[href$='.jpg'], a[href$='.png'], a[href$='.jpeg']").length >= 1) {
      $("a[href$='.jpg'], a[href$='.png'], a[href$='.jpeg']").iLightBox({
        path: "horizontal",
        skin: "dark",
        controls: { arrows: true }
      });
    }

    // Javascript Breakpoint function usage: if( breakpointBetween("768px", "992px") ){}
    function breakpointBetween(min, max){
      if( window.matchMedia("(min-width: "+min+") and (max-width: "+max+")").matches ){
        return true;
      }else{
        return false;
      }
    }
    
    //Call functions on Resize
    //$(window).resize(function() {});
  });
});
