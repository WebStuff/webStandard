<?php
/*
Add to the top of your php page include_once('minifyhtml.php');
*/
$enable_page_create = false; //enabling this will create a file for the minified page.  add the variable $permalink = 'index.html'; above the include for each page.
function minify_output($buffer)
{
    global $enable_page_create, $permalink;
$search = array(
'/\>[^\S ]+/s',
'/[^\S ]+\</s',
'/(\s)+/s'
);
$replace = array(
'>',
'<',
'\\1'
);
if (preg_match("/\<html/i",$buffer) == 1 && preg_match("/\<\/html\>/i",$buffer) == 1) {
$buffer = preg_replace($search, $replace, $buffer);
}
if($enable_page_create){
    if( !$permalink ) $permalink = 'index.html';
    $f = fopen($permalink, "w");
    fwrite($f, $buffer);
    fclose($f);
}
return $buffer;
}
ob_start("minify_output");
