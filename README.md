# webStandard - imageDESIGN

_version 1.2_

Use these files to start your web project

## Libraries Included

- [Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/) - The web framework used
- [Glyphicons](http://glyphicons.com/) - Icons and symbols
- [Slick](http://kenwheeler.github.io/slick/) - Responsive carousel/slider
- [iLightbox](http://ilightbox.net/documentation.html) - Lighbox overlay windows
- [AOS](https://michalsnik.github.io/aos/) - Animate On Scroll
- [Animate.css](https://daneden.github.io/animate.css/) - Prebuilt CSS animations

## Launch Items

### Setup .htaccess File
Create a file named .htaccess in the root/main folder of the website and add the following:
```
# php -- BEGIN cPanel-generated handler, do not edit
# Set the “ea-php74” package as the default “PHP” programming language.
<IfModule mime_module>
  AddType application/x-httpd-ea-php74 .php .php7 .phtml
</IfModule>
# php -- END cPanel-generated handler, do not edit

<IfModule mod_headers.c>
  Header set X-XSS-Protection "1; mode=block"
  Header always append X-Frame-Options SAMEORIGIN
  Header set X-Content-Type-Options nosniff
</IfModule>

# Turn on Expires and set default to 0
ExpiresActive On
ExpiresDefault A0
 
# Set up caching on media files for 1 year
<filesMatch "\.(flv|ico|pdf|avi|mov|ppt|doc|mp3|wmv|wav)$">
Header set Cache-Control "max-age=29030400, public"
ExpiresDefault "access plus 1 years"
Header unset Last-Modified
Header unset ETag
SetOutputFilter DEFLATE
</filesMatch>
 
# Set up caching on media files for 1 month
<filesMatch "\.(gif|jpg|jpeg|png|swf|svg|pdf|eot|ttf|woff|woff2|icon|icn)$">
Header set Cache-Control "max-age=2419200, public"
ExpiresDefault "access plus 1 month"
SetOutputFilter DEFLATE
</filesMatch>
 
# Set up caching on text files for 1 week
<filesMatch "\.(css|js|txt|xml|json)$">
Header set Cache-Control "max-age=604800, public"
ExpiresDefault "access plus 1 week"
SetOutputFilter DEFLATE
</filesMatch>

# Force no caching for dynamic files
<filesMatch "\.(php|cgi|pl)$">
ExpiresActive Off
Header set Cache-Control "private, no-cache, no-store, proxy-revalidate, no-transform"
Header set Pragma "no-cache"
SetOutputFilter DEFLATE
</filesMatch>

# ----------------------------------------------------------------------
# Webfont access
# ----------------------------------------------------------------------

# Allow access from all domains for webfonts.

<IfModule mod_headers.c>
  Header set X-Content-Type-Options nosniff
  Header set X-XSS-Protection "1; mode=block"
  <FilesMatch "\.(eot|font.css|otf|ttc|ttf|woff|css|svg|woff2)$">
    Header set Access-Control-Allow-Origin "*"
  </FilesMatch>
</IfModule>

# ----------------------------------------------------------------------
# ETag removal
# ----------------------------------------------------------------------

# FileETag None is not enough for every server.
<IfModule mod_headers.c>
  Header unset ETag
</IfModule>

# Since we're sending far-future expires, we don't need ETags for static content.
FileETag None

<IfModule mod_headers.c>
# Using DENY will block all iFrames including iFrames on your own website
# Header set X-Frame-Options DENY
# Recommended: SAMEORIGIN - iFrames from the same site are allowed - other sites are blocked
# Block other sites from displaying your website in iFrames
# Protects against Clickjacking
Header always append X-Frame-Options SAMEORIGIN
# Protects against Drive-by Download attacks
# Protects against MIME/Content/Data sniffing
Header set X-Content-Type-Options nosniff
</IfModule>
```

### Remove .html/.php extension from pages
Add to bottom of .htaccess file:
```
# Serve HTML/PHP website without .html/.php (change extention in last line)
RewriteEngine On
#uncomment next line to add any directories you may need access to below
#RewriteRule ^(dirname|dirname2)($|/) - [L]
#Adds 301 redirect on all .html or .php pages to automatically remove extension
RewriteCond %{THE_REQUEST} \ /(.+)\.(html|php)
RewriteRule ^ /%1 [L,R=301]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^([^\.]+)$ $1.php [NC,L]
```
### Force HTTPS
Add to bottom of .htaccess file:
```
#Force HTTPS
RewriteEngine On 
RewriteCond %{SERVER_PORT} 80 
RewriteRule ^(.*)$ https://domain.com/$1 [R,L]
```
### Test Facebook Share image
[Facebook Debug Object](https://developers.facebook.com/tools/debug//)

### Create SiteMap.xml
Go to [www.xml-sitemaps.com](https://www.xml-sitemaps.com/) and add files to root/main site folder

### Analytics Dashboard
After adding Google Analytics, set up the custom dashboard to email monthly to client at [Custom Dashboard](https://www.google.com/analytics/web/template?uid=6E2X5-aXSYOZOyTGgeKWVQ)

### Convert transparent PNG photos to SVG
[JPNG.svg](https://codepen.io/shshaw/full/LVKEdv)

### Generate Favicon
1.  Save out a square icon image at 1024x1024px as a PNG
2.  Go to [realfavicongenerator.net](https://realfavicongenerator.net/) and upload the image and adjust settings to create your favicon images

### Speed Test
Test the site's home page on the two services below and do any recommendations to increase your page speed score.
* [GTMetrix](https://gtmetrix.com/)
* [PageSpeed](https://developers.google.com/speed/pagespeed/insights/)

